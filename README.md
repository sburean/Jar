Jar application: Intended to help users break bad habits by having them incur a cost each time they slip up, up to a self-imposed total amount. 
Users may invite others to join their cause.